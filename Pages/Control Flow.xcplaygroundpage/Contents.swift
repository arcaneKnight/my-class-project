//: [Previous](@previous) Collections
/*:
# Control Flow

Every language has some way to control the execution of an app. In Swift, we have:
* `if` and `switch` for conditionals
* `for`, `while` for loops
* `for-in` as a shortcut to loop through elements in a collection

Reminder:
* When dealing with `while` loops, you are responsible for conditions to end the loop, or to `break` it, otherwise your program will run in an endless loop

*/

import Foundation

// if conditionals
var score: Int = 49

if score > 50 {
    print("Pass")
} else {
    print("Fail")
}

// loops
var colors: [String] = ["Red", "Green", "Blue"]
for (var i = 0; i < colors.count; i++) {
    print("Let's paint the town \(colors[i])")
}

for color in colors {
    print("Let's paint the town \(color)")
}

// While loop
var videoCount = 0
while (videoCount < 3) {
    print("Watched \(videoCount) videos")
    videoCount = videoCount + 1
}


//: [Next](@next) Functions 

//: [Previous](@previous)
/*:
# Assignment: Snake & Ladders
Now that you've got some idea how Swift looks like, it's time for you to get your hands dirty doing the first Swift assignment.

Build a simple Snake & Ladders game. You'll need to have some game setup function, which keeps a board of 100 squares. Some of the squares are "Snakes", meaning that a player who lands on the square will fall back a number of positions. Some of the squares are "Ladders", where a player who lands there will go forward a number of positions. The goal, is to be the first to reach the 100th square. You can set the number of players as a variable, anything between 2 to 5 makes a good number.

The main loop will basically roll the dice for each player, and print out the their current position, rolled dice, new position, and effect of landing on a snake or ladder.

No user input is expected for this simple assignment. The program just has to keep on running until one player reaches the goal.
*/
import Foundation

// Initial variables (just to get you started)
var numberOfPlayers = 2
var board: [Int] = []

// Functions
func boardSetup() {
    
}

func rollDice() -> Int {
    // Generate a random number between 1 to 6
    // Hint: use the rand() function to get a random number, then limit it to between 1 to 6
    return 0
}

// Pass it a player number, rolled dice, etc 
// and print out a description of the move
func printStep() {
    
}

boardSetup()
var hasWinner: Bool = false

while (hasWinner) {
    // For each player, find the current position, roll the dice,
    // set the new position based on the board's snakes and ladders
    // print out the move
    rollDice()
    
    // Check for winners
    
    // Repeat until winner is found
    hasWinner = true
}


//: [Next](@next)

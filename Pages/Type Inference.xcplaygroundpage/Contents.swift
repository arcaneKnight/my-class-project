//: [Previous](@previous) Data Types
/*:

# Type Inference

How does Swift know what the data type is? Well, it "infers" the data type based on the initial value you give when you first assign the constant or variable.

Another is for you, the developer, to explicitly state the data type using "type annotation" when you first declare the constant or variable.
* If you use type annotation, Swift will convert the data type (if possible) when you assign the value
* An added benefit for using type annotation is that Xcode will highlight if the value doesn't match the data type

TIP: For the duration of this course (and until you and your team are very familiar with Swift language and the entire iOS framework), we strongly encourage you to use type annotation all the time.

*/
import Foundation

var videoName: String = "Swift Tutorial"
var videoLength: Double = 10
var video2Length: Double = 3.7

var totalLength: Double = videoLength + video2Length

var randomDecimal: Double = rand()


totalLength = videoLength + randomDecimal

//: [Next](@next) Collections
